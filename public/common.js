async function loadHeader() {
  let header = document.querySelector('header');
  let res = await fetch('/header.html');
  let html = await res.text();
  header.outerHTML = html;
}
loadHeader();

async function loadFooter() {
  let footer = document.querySelector('footer');
  let res = await fetch('/footer.html');
  let html = await res.text();
  footer.outerHTML = html;
}
loadFooter();
