# multi-page-web

This repo demo how to share the same header and footer for multiple pages using `fetch()` on the html files.

Example see [home-page.html](./public/home-page.html)

Bonus: also checkout [single-page-web](../../../../single-page-web) to avoid refreshing the pages

## Get Started

You can run a web server with node.js:
```bash
npx http-server
```

or python 3:
```bash
python -m http.server --directory public
```

or python 2:
```bash
cd public
python -m SimpleHTTPServer
```
